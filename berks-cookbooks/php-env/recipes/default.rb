#
# Cookbook Name:: php-env
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
%w{php-fpm php-pecl-zendopcache}.each do |pkg|
  package pkg do
    action :install
    notifies :restart, "service[php-fpm]"
  end
end

service "php-fpm" do
  action [:enable, :start]
end

template 'index.php' do
  path '/usr/share/nginx/html/index.php'
  source "index.php.erb"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, "service[php-fpm]"
end
